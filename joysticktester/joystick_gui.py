#!/usr/bin/python3
import logging
import os
import threading
import time
import tkinter

import pkg_resources
from PIL import Image
from PIL import ImageTk
from screeninfo import get_monitors

logger = logging.getLogger(__name__)

resource_package = pkg_resources.get_distribution('joysticktester').location

back_ground_joystick = os.path.join(
    resource_package, 'joysticktester', 'assets', 'joystick.png')


class JoystickGui:
    def __init__(self, **kargs):
        self.master = kargs['master']
        self.event_listener = kargs['event_listener']
        monitor1 = get_monitors()[0]
        self.screen_width = monitor1.width
        self.screen_height = monitor1.height
        self.width = self.screen_width / 2  # width for the Tk widget
        self.height = self.screen_height / 2
        self.x_pos = self.screen_width / 4
        self.y_pos = self.screen_height / 4
        self.main_canvas = tkinter.Canvas(
            self.master, width=self.width, height=self.height)
        self.main_canvas.pack()
        # self.master.attributes('-fullscreen', True)
        self.master.geometry('%dx%d+%d+%d' %
                             (self.width, self.height, self.x_pos, self.y_pos))
        self.master.resizable(False, False)

        self.button_released_color = 'red'
        self.button_pressed_color = 'green'
        self.button_dodgy_color = 'orange'
        self.clicks_enabled = True
        self.need_to_clear_gui = False
        self.button_font_size = int(self.screen_width * 0.00625)
        self.title_font_size = int(self.screen_width * 0.010416666666666666)
        self.buttons_dic = {}
        self.canvas_image_dic = {}
        self._create_image_object(width=self.width, height=self.height, image_path=back_ground_joystick)
        self._create_joystick_buttons()
        self._create_title_label()

        self._create_finish_button()
        self._create_restart_button()

        self.event_listener.set_gui_controller(gui_controller=self)

    def _create_title_label(self):
        finish_label_xstart = self.screen_width * 0.25
        finish_label_ystart = self.screen_height * 0.027777777777777776

        self.title_label = self.main_canvas.create_text(finish_label_xstart, finish_label_ystart,
                                                        text="",
                                                        font=('Lato Regular', self.title_font_size), fill='blue',
                                                        anchor=tkinter.CENTER)

    def _create_finish_button(self):

        finish_rectangle_width = self.screen_width * 0.0625
        finish_rectangle_height = self.screen_height * 0.037037037037037035
        finish_rectangle_xstart = self.screen_width * 0.4010416666666667
        finish_rectangle_ystart = self.screen_height * 0.4444444444444444
        finish_rectangle_xend = finish_rectangle_xstart + finish_rectangle_width
        finish_rectangle_yend = finish_rectangle_ystart + finish_rectangle_height

        self.finish_rectangle = self.main_canvas.create_rectangle(finish_rectangle_xstart, finish_rectangle_ystart,
                                                                  finish_rectangle_xend,
                                                                  finish_rectangle_yend, fill='grey',
                                                                  width=self.screen_width * 0.0020833333333333333,
                                                                  outline='#212122')

        finish_label_xstart = finish_rectangle_xstart + self.screen_width * 0.005208333333333333
        finish_label_ystart = finish_rectangle_ystart + self.screen_height * 0.01574074074074074

        self.finish_label = self.main_canvas.create_text(finish_label_xstart, finish_label_ystart,
                                                         text="finish testing",
                                                         font=('Lato Regular', self.button_font_size), fill='black',
                                                         anchor=tkinter.W)

        self.finish_rectangle_exec = self.main_canvas.create_rectangle(finish_rectangle_xstart, finish_rectangle_ystart,
                                                                       finish_rectangle_xend,
                                                                       finish_rectangle_yend, fill='',
                                                                       outline='')

        self.main_canvas.tag_bind(self.finish_rectangle_exec, '<Enter>', self._enter_finish_button)
        self.main_canvas.tag_bind(self.finish_rectangle_exec, '<Leave>', self._leave_finish_button)
        self.main_canvas.tag_bind(self.finish_rectangle_exec, '<Button-1>', self.trigger_finish)

    def _enter_finish_button(self, event=None):
        self.main_canvas.itemconfig(self.finish_rectangle, fill='#07a5c9')

    def _leave_finish_button(self, event=None):
        self.main_canvas.itemconfig(self.finish_rectangle, fill='grey')

    def trigger_finish(self, event=None):
        thread_show_results = threading.Thread(target=self.show_results)
        thread_show_results.start()
        self.need_to_clear_gui = True

    def show_results(self):
        self.clicks_enabled = False
        for key in self.buttons_dic:
            button = self.buttons_dic[key].button
            active = self.buttons_dic[key].working
            dodgy = self.buttons_dic[key].dodgy
            if dodgy:
                self.button_dodgy(button=button)
            elif active:
                self.button_pressed(button=button)
            else:
                self.button_released(button=button)
        self.set_title(text="test results")
        self._create_explanation_rectangles()

    def _create_explanation_rectangles(self):
        rectangle_width = self.screen_width * 0.041666666666666664
        rectangle_height = self.screen_height * 0.018518518518518517

        green_rectangle_xstart = self.screen_width * 0.026041666666666668
        green_rectangle_ystart = self.screen_height * 0.42592592592592593
        green_rectangle_xend = green_rectangle_xstart + rectangle_width
        green_rectangle_yend = green_rectangle_ystart + rectangle_height

        self.green_rectangle = self.main_canvas.create_rectangle(green_rectangle_xstart, green_rectangle_ystart,
                                                                 green_rectangle_xend,
                                                                 green_rectangle_yend, fill='green',
                                                                 width=self.screen_width * 0.0020833333333333333,
                                                                 outline='#212122')
        orange_rectangle_xstart = self.screen_width * 0.026041666666666668
        orange_rectangle_ystart = self.screen_height * 0.45092592592592595
        orange_rectangle_xend = orange_rectangle_xstart + rectangle_width
        orange_rectangle_yend = orange_rectangle_ystart + rectangle_height

        self.orange_rectangle = self.main_canvas.create_rectangle(orange_rectangle_xstart, orange_rectangle_ystart,
                                                                  orange_rectangle_xend,
                                                                  orange_rectangle_yend, fill='orange',
                                                                  width=self.screen_width * 0.0020833333333333333,
                                                                  outline='#212122')
        red_rectangle_xstart = self.screen_width * 0.026041666666666668
        red_rectangle_ystart = self.screen_height * 0.4759259259259259
        red_rectangle_xend = red_rectangle_xstart + rectangle_width
        red_rectangle_yend = red_rectangle_ystart + rectangle_height

        self.red_rectangle = self.main_canvas.create_rectangle(red_rectangle_xstart, red_rectangle_ystart,
                                                               red_rectangle_xend,
                                                               red_rectangle_yend, fill='red',
                                                               width=self.screen_width * 0.0020833333333333333,
                                                               outline='#212122')

        green_label_xstart = self.screen_width * 0.07552083333333333
        green_label_ystart = self.screen_height * 0.43703703703703706

        self.green_rec_label = self.main_canvas.create_text(green_label_xstart, green_label_ystart,
                                                            text="worked",
                                                            font=('Lato Regular', self.button_font_size), fill='black',
                                                            anchor=tkinter.W)

        orange_label_xstart = self.screen_width * 0.07552083333333333
        orange_label_ystart = self.screen_height * 0.462037037037037

        self.orange_rec_label = self.main_canvas.create_text(orange_label_xstart, orange_label_ystart,
                                                             text="might be dodgy",
                                                             font=('Lato Regular', self.button_font_size), fill='black',
                                                             anchor=tkinter.W)

        red_label_xstart = self.screen_width * 0.07552083333333333
        red_label_ystart = self.screen_height * 0.48703703703703705

        self.red_rec_label = self.main_canvas.create_text(red_label_xstart, red_label_ystart,
                                                          text="didn't worked",
                                                          font=('Lato Regular', self.button_font_size), fill='black',
                                                          anchor=tkinter.W)

    def set_title(self, **kwargs):
        text = kwargs['text']
        self.main_canvas.itemconfig(self.title_label,
                                    text=text, anchor=tkinter.CENTER)

    def _create_restart_button(self):

        restart_rectangle_width = self.screen_width * 0.0625
        restart_rectangle_height = self.screen_height * 0.037037037037037035
        restart_rectangle_xstart = self.screen_width * 0.328125
        restart_rectangle_ystart = self.screen_height * 0.4444444444444444
        restart_rectangle_xend = restart_rectangle_xstart + restart_rectangle_width
        restart_rectangle_yend = restart_rectangle_ystart + restart_rectangle_height

        self.restart_rectangle = self.main_canvas.create_rectangle(restart_rectangle_xstart, restart_rectangle_ystart,
                                                                   restart_rectangle_xend,
                                                                   restart_rectangle_yend, fill='grey',
                                                                   width=self.screen_width * 0.0020833333333333333,
                                                                   outline='#212122')

        restart_label_xstart = restart_rectangle_xstart + self.screen_width * 0.0078125
        restart_label_ystart = restart_rectangle_ystart + self.screen_height * 0.01574074074074074

        self.restart_label = self.main_canvas.create_text(restart_label_xstart, restart_label_ystart,
                                                          text="restart test",
                                                          font=('Lato Regular', self.button_font_size), fill='black',
                                                          anchor=tkinter.W)

        self.restart_rectangle_exec = self.main_canvas.create_rectangle(restart_rectangle_xstart,
                                                                        restart_rectangle_ystart,
                                                                        restart_rectangle_xend,
                                                                        restart_rectangle_yend, fill='',
                                                                        outline='')

        self.main_canvas.tag_bind(self.restart_rectangle_exec, '<Enter>', self._enter_restart_button)
        self.main_canvas.tag_bind(self.restart_rectangle_exec, '<Leave>', self._leave_restart_button)
        self.main_canvas.tag_bind(self.restart_rectangle_exec, '<Button-1>', self.trigger_restart)

    def _enter_restart_button(self, event=None):
        self.main_canvas.itemconfig(self.restart_rectangle, fill='#07a5c9')

    def _leave_restart_button(self, event=None):
        self.main_canvas.itemconfig(self.restart_rectangle, fill='grey')

    def trigger_restart(self, event=None):
        self.clicks_enabled = True
        time_init = time.time()
        for key in self.buttons_dic:
            self.buttons_dic[key].working = False
            self.buttons_dic[key].dodgy = False
            self.buttons_dic[key].time_pressed = time_init
            self.buttons_dic[key].time_released = time_init - 1

            self.button_released(button=self.buttons_dic[key].button)
        self.set_title(text="start clicking buttons")
        self._clear_explanation_rectangles()

    def _clear_explanation_rectangles(self):
        if self.need_to_clear_gui:
            self.main_canvas.delete(self.green_rectangle)
            self.main_canvas.delete(self.orange_rectangle)
            self.main_canvas.delete(self.red_rectangle)
            self.main_canvas.delete(self.green_rec_label)
            self.main_canvas.delete(self.orange_rec_label)
            self.main_canvas.delete(self.red_rec_label)

    def _create_joystick_buttons(self):
        self.button0_xstart = self.screen_width * 0.3640625
        self.button0_ystart = self.screen_height * 0.14074074074074075

        self.button1_xstart = self.screen_width * 0.39479166666666665
        self.button1_ystart = self.screen_height * 0.18425925925925926

        self.button2_xstart = self.screen_width * 0.3640625
        self.button2_ystart = self.screen_height * 0.22777777777777777

        self.button3_xstart = self.screen_width * 0.3328125
        self.button3_ystart = self.screen_height * 0.18425925925925926

        self.button_small_radius_width = self.screen_width * 0.010416666666666666
        self.button_small_radius_height = self.screen_height * 0.014814814814814815

        self.button0 = self.main_canvas.create_oval(self.button0_xstart - self.button_small_radius_width,
                                                    self.button0_ystart - self.button_small_radius_height,
                                                    self.button0_xstart + self.button_small_radius_width,
                                                    self.button0_ystart + self.button_small_radius_height,
                                                    fill=self.button_released_color,
                                                    width=1,
                                                    outline='#212122')

        self.button1 = self.main_canvas.create_oval(self.button1_xstart - self.button_small_radius_width,
                                                    self.button1_ystart - self.button_small_radius_height,
                                                    self.button1_xstart + self.button_small_radius_width,
                                                    self.button1_ystart + self.button_small_radius_height,
                                                    fill=self.button_released_color,
                                                    width=1,
                                                    outline='#212122')

        self.button2 = self.main_canvas.create_oval(self.button2_xstart - self.button_small_radius_width,
                                                    self.button2_ystart - self.button_small_radius_height,
                                                    self.button2_xstart + self.button_small_radius_width,
                                                    self.button2_ystart + self.button_small_radius_height,
                                                    fill=self.button_released_color,
                                                    width=1,
                                                    outline='#212122')

        self.button3 = self.main_canvas.create_oval(self.button3_xstart - self.button_small_radius_width,
                                                    self.button3_ystart - self.button_small_radius_height,
                                                    self.button3_xstart + self.button_small_radius_width,
                                                    self.button3_ystart + self.button_small_radius_height,
                                                    fill=self.button_released_color,
                                                    width=1,
                                                    outline='#212122')

        self.button_rectangle_width = self.screen_width * 0.026041666666666668
        self.button_rectangle_height = self.screen_height * 0.009259259259259259
        self.button_rectangle_outline_width = self.screen_width * 0.0015625

        self.button4_xstart = self.screen_width * 0.125
        self.button4_ystart = self.screen_height * 0.08518518518518518
        self.button4_xend = self.button4_xstart + self.button_rectangle_width
        self.button4_yend = self.button4_ystart + self.button_rectangle_height

        self.button5_xstart = self.screen_width * 0.35
        self.button5_ystart = self.screen_height * 0.08518518518518518
        self.button5_xend = self.button5_xstart + self.button_rectangle_width
        self.button5_yend = self.button5_ystart + self.button_rectangle_height

        self.button6_xstart = self.screen_width * 0.125
        self.button6_ystart = self.screen_height * 0.07222222222222222
        self.button6_xend = self.button6_xstart + self.button_rectangle_width
        self.button6_yend = self.button6_ystart + self.button_rectangle_height

        self.button7_xstart = self.screen_width * 0.35
        self.button7_ystart = self.screen_height * 0.07222222222222222
        self.button7_xend = self.button7_xstart + self.button_rectangle_width
        self.button7_yend = self.button7_ystart + self.button_rectangle_height

        self.button4 = self.main_canvas.create_rectangle(self.button4_xstart, self.button4_ystart, self.button4_xend,
                                                         self.button4_yend, fill=self.button_released_color,
                                                         width=self.button_rectangle_outline_width,
                                                         outline='#212122')
        self.button5 = self.main_canvas.create_rectangle(self.button5_xstart, self.button5_ystart, self.button5_xend,
                                                         self.button5_yend, fill=self.button_released_color,
                                                         width=self.button_rectangle_outline_width,
                                                         outline='#212122')

        self.button6 = self.main_canvas.create_rectangle(self.button6_xstart, self.button6_ystart, self.button6_xend,
                                                         self.button6_yend, fill=self.button_released_color,
                                                         width=self.button_rectangle_outline_width,
                                                         outline='#212122')

        self.button7 = self.main_canvas.create_rectangle(self.button7_xstart, self.button7_ystart, self.button7_xend,
                                                         self.button7_yend, fill=self.button_released_color,
                                                         width=self.button_rectangle_outline_width,
                                                         outline='#212122')

        self.button_select_width = self.screen_width * 0.0109375
        self.button_select_height = self.screen_height * 0.007407407407407408
        self.button8_xstart = self.screen_width * 0.21041666666666667
        self.button8_ystart = self.screen_height * 0.17962962962962964
        self.button8_xend = self.button8_xstart + self.button_select_width
        self.button8_yend = self.button8_ystart + self.button_select_height

        self.button8 = self.main_canvas.create_rectangle(self.button8_xstart, self.button8_ystart, self.button8_xend,
                                                         self.button8_yend, fill=self.button_released_color,
                                                         outline='#212122')

        self.button9_x0 = self.screen_width * 0.27760416666666665
        self.button9_y0 = self.screen_height * 0.17962962962962964

        self.button9_x1 = self.screen_width * 0.27760416666666665
        self.button9_y1 = self.screen_height * 0.18703703703703703

        self.button9_x2 = self.screen_width * 0.28854166666666664
        self.button9_y2 = self.screen_height * 0.18333333333333332

        self.button9 = self.main_canvas.create_polygon(
            (self.button9_x0, self.button9_y0, self.button9_x1, self.button9_y1, self.button9_x2, self.button9_y2),
            fill=self.button_released_color, outline='#212122')

        self.button10_xstart = self.screen_width * 0.19427083333333334
        self.button10_ystart = self.screen_height * 0.26296296296296295

        self.button11_xstart = self.screen_width * 0.30885416666666665
        self.button11_ystart = self.screen_height * 0.26296296296296295

        self.button_large_radius_width = self.screen_width * 0.021354166666666667
        self.button_large_radius_height = self.screen_height * 0.02962962962962963

        self.button10 = self.main_canvas.create_oval(self.button10_xstart - self.button_large_radius_width,
                                                     self.button10_ystart - self.button_large_radius_height,
                                                     self.button10_xstart + self.button_large_radius_width,
                                                     self.button10_ystart + self.button_large_radius_height,
                                                     fill=self.button_released_color,
                                                     width=1,
                                                     outline='#212122')

        self.button11 = self.main_canvas.create_oval(self.button11_xstart - self.button_large_radius_width,
                                                     self.button11_ystart - self.button_large_radius_height,
                                                     self.button11_xstart + self.button_large_radius_width,
                                                     self.button11_ystart + self.button_large_radius_height,
                                                     fill=self.button_released_color,
                                                     width=self.screen_width * 0.0020833333333333333,
                                                     outline='#212122')
        # axis0
        self.axis0_x0 = self.screen_width * 0.165625
        self.axis0_y0 = self.screen_height * 0.1712962962962963

        self.axis0_x1 = self.screen_width * 0.165625
        self.axis0_y1 = self.screen_height * 0.1935185185185185

        self.axis0_x2 = self.screen_width * 0.153125
        self.axis0_y2 = self.screen_height * 0.1935185185185185

        self.axis0_x3 = self.screen_width * 0.1453125
        self.axis0_y3 = self.screen_height * 0.1824074074074074

        self.axis0_x4 = self.screen_width * 0.153125
        self.axis0_y4 = self.screen_height * 0.1712962962962963

        # axis1
        self.axis1_x0 = self.screen_width * 0.14635416666666667
        self.axis1_y0 = self.screen_height * 0.2212962962962963

        self.axis1_x1 = self.screen_width * 0.1296875
        self.axis1_y1 = self.screen_height * 0.2212962962962963

        self.axis1_x2 = self.screen_width * 0.1296875
        self.axis1_y2 = self.screen_height * 0.20462962962962963

        self.axis1_x3 = self.screen_width * 0.13802083333333334
        self.axis1_y3 = self.screen_height * 0.1935185185185185

        self.axis1_x4 = self.screen_width * 0.14635416666666667
        self.axis1_y4 = self.screen_height * 0.20462962962962963

        # axis2
        self.axis2_x0 = self.screen_width * 0.109375
        self.axis2_y0 = self.screen_height * 0.1712962962962963

        self.axis2_x1 = self.screen_width * 0.109375
        self.axis2_y1 = self.screen_height * 0.1935185185185185

        self.axis2_x2 = self.screen_width * 0.121875
        self.axis2_y2 = self.screen_height * 0.1935185185185185

        self.axis2_x3 = self.screen_width * 0.13020833333333334
        self.axis2_y3 = self.screen_height * 0.1824074074074074

        self.axis2_x4 = self.screen_width * 0.121875
        self.axis2_y4 = self.screen_height * 0.1712962962962963

        # axis3
        self.axis3_x0 = self.screen_width * 0.14635416666666667
        self.axis3_y0 = self.screen_height * 0.1398148148148148

        self.axis3_x1 = self.screen_width * 0.1296875
        self.axis3_y1 = self.screen_height * 0.1398148148148148

        self.axis3_x2 = self.screen_width * 0.1296875
        self.axis3_y2 = self.screen_height * 0.15648148148148147

        self.axis3_x3 = self.screen_width * 0.13802083333333334
        self.axis3_y3 = self.screen_height * 0.1685185185185185

        self.axis3_x4 = self.screen_width * 0.14635416666666667
        self.axis3_y4 = self.screen_height * 0.15648148148148147

        self.axis0 = self.main_canvas.create_polygon((self.axis0_x0, self.axis0_y0, self.axis0_x1, self.axis0_y1,
                                                      self.axis0_x2, self.axis0_y2, self.axis0_x3, self.axis0_y3,
                                                      self.axis0_x4, self.axis0_y4), fill=self.button_released_color,
                                                     outline='#212122')

        self.axis1 = self.main_canvas.create_polygon((self.axis1_x0, self.axis1_y0, self.axis1_x1, self.axis1_y1,
                                                      self.axis1_x2, self.axis1_y2, self.axis1_x3, self.axis1_y3,
                                                      self.axis1_x4, self.axis1_y4), fill=self.button_released_color,
                                                     outline='#212122')

        self.axis2 = self.main_canvas.create_polygon((self.axis2_x0, self.axis2_y0, self.axis2_x1, self.axis2_y1,
                                                      self.axis2_x2, self.axis2_y2, self.axis2_x3, self.axis2_y3,
                                                      self.axis2_x4, self.axis2_y4), fill=self.button_released_color,
                                                     outline='#212122')

        self.axis3 = self.main_canvas.create_polygon((self.axis3_x0, self.axis3_y0, self.axis3_x1, self.axis3_y1,
                                                      self.axis3_x2, self.axis3_y2, self.axis3_x3, self.axis3_y3,
                                                      self.axis3_x4, self.axis3_y4), fill=self.button_released_color,
                                                     outline='#212122')

        # adding buttons to a dictionary
        init_time = time.time()
        self.buttons_dic["axis0"] = ButtonEvent(button=self.axis0, working=False, dodgy=False, time_pressed=init_time,
                                                time_released=init_time - 1)
        self.buttons_dic["axis1"] = ButtonEvent(button=self.axis1, working=False, dodgy=False, time_pressed=init_time,
                                                time_released=init_time - 1)
        self.buttons_dic["axis2"] = ButtonEvent(button=self.axis2, working=False, dodgy=False, time_pressed=init_time,
                                                time_released=init_time - 1)
        self.buttons_dic["axis3"] = ButtonEvent(button=self.axis3, working=False, dodgy=False, time_pressed=init_time,
                                                time_released=init_time - 1)
        self.buttons_dic["button0"] = ButtonEvent(button=self.button0, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button1"] = ButtonEvent(button=self.button1, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button2"] = ButtonEvent(button=self.button2, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button3"] = ButtonEvent(button=self.button3, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button4"] = ButtonEvent(button=self.button4, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button5"] = ButtonEvent(button=self.button5, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button6"] = ButtonEvent(button=self.button6, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button7"] = ButtonEvent(button=self.button7, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button8"] = ButtonEvent(button=self.button8, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button9"] = ButtonEvent(button=self.button9, working=False, dodgy=False,
                                                  time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button10"] = ButtonEvent(button=self.button10, working=False, dodgy=False,
                                                   time_pressed=init_time, time_released=init_time - 1)
        self.buttons_dic["button11"] = ButtonEvent(button=self.button11, working=False, dodgy=False,
                                                   time_pressed=init_time, time_released=init_time - 1)

    def _create_image_object(self, **kargs):
        width = kargs['width']
        height = kargs['height']
        image_path = kargs['image_path']
        self.img = Image.open(image_path)
        self.img = self.img.resize(
            (int(width * 0.75), int(height * 0.75)), Image.ANTIALIAS)
        self.canvas_image_dic[image_path] = ImageTk.PhotoImage(self.img)

        new_image = self.main_canvas.create_image(self.width / 8, self.height / 8,
                                                  image=self.canvas_image_dic[image_path],
                                                  anchor=tkinter.NW)
        return new_image

    def button_dodgy(self, **kwargs):
        button = kwargs['button']
        self.main_canvas.itemconfig(button, fill=self.button_dodgy_color)

    def button_pressed(self, **kwargs):
        button = kwargs['button']
        self.main_canvas.itemconfig(button, fill=self.button_pressed_color)

    def button_released(self, **kwargs):
        button = kwargs['button']
        self.main_canvas.itemconfig(button, fill=self.button_released_color)

    def update_buttons(self, **kargs):

        if not self.clicks_enabled:
            return

        try:
            buttons_dic = kargs['buttons_dictionary']
            for key, value in buttons_dic.items():
                event_time = time.time()
                button_events = self.buttons_dic[key]
                if button_events.dodgy:
                    continue

                if value:
                    button_events.working = True
                    button_events.time_pressed = event_time
                    if button_events.time_pressed - button_events.time_released <= 0.1:
                        button_events.dodgy = True
                        self.button_dodgy(button=button_events.button)
                        continue

                    self.button_pressed(button=button_events.button)
                else:
                    if button_events.time_released < button_events.time_pressed:
                        button_events.time_released = event_time

        except KeyError:
            #logging.warning("a wrong key ({}) has been sent to update_buttons in joystick_gui".format(key))
            pass # mean while pass


class ButtonEvent:
    def __init__(self, **kargs):
        self.button = kargs['button']
        self.working = kargs['working']
        self.dodgy = kargs['dodgy']
        self.time_pressed = kargs['time_pressed']
        self.time_released = kargs['time_released']


class GuiExecuter(object):
    """ Threading class
    The run() method will be started and it will run in the background
    until the application exits.
    """

    def __init__(self, **kargs):
        self.event_listener = kargs['event_listener']
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True  # Daemonize thread
        self.thread.start()  # Start the execution

    def run(self):
        """ Method that runs forever """

        widget = tkinter.Tk()
        widget.title("Joystick Tester")
        self._gui = JoystickGui(master=widget, event_listener=self.event_listener)
        widget.mainloop()
