import logging
import os
import sys
import threading
import time

logger = logging.getLogger(__name__)
sys.stdout = open(os.devnull, "w")

import pygame

sys.stdout = sys.__stdout__


class EventListener(threading.Thread):
    def __init__(self):
        super(EventListener, self).__init__()
        self.done = False
        self.axis0 = "axis0"
        self.axis1 = "axis1"
        self.axis2 = "axis2"
        self.axis3 = "axis3"
        self.button0 = "button0"
        self.button1 = "button1"
        self.button2 = "button2"
        self.button3 = "button3"
        self.button4 = "button4"
        self.button5 = "button5"
        self.button6 = "button6"
        self.button7 = "button7"
        self.button8 = "button8"
        self.button9 = "button9"
        self.button10 = "button10"
        self.button11 = "button11"
        self.joystick_buttons = {
            self.axis0: False,
            self.axis1: False,
            self.axis2: False,
            self.axis3: False,
            self.button0: False,
            self.button1: False,
            self.button2: False,
            self.button3: False,
            self.button4: False,
            self.button5: False,
            self.button6: False,
            self.button7: False,
            self.button8: False,
            self.button9: False,
            self.button10: False,
            self.button11: False
        }
        self.gui_controller = None
        self.inactivity_reconnect_time = 5
        self.reconnect_timeout = 1
        self.last_time_not_connected = 0
        self.last_time_pressed = 0
        self.last_time_streamed_events = 0
        self.need_to_clear = False

        pygame.init()

        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True
        self.thread.start()

    def set_gui_controller(self, **kwargs):
        self.gui_controller = kwargs['gui_controller']

    def has_controller(self):
        now = time.time()
        if (
                now - self.last_time_pressed > self.inactivity_reconnect_time or now - self.last_time_streamed_events > self.inactivity_reconnect_time) and now - self.last_time_not_connected > self.reconnect_timeout:
            self.last_time_not_connected = now
            pygame.joystick.quit()
            pygame.joystick.init()

        return pygame.joystick.get_count() > 0

    def run(self):
        while not self.done:
            # EVENT PROCESSING STEP
            self.is_streaming_events = False
            for event in pygame.event.get():  # User did something
                if event is not None:
                    self.last_time_streamed_events = time.time()

            if not self.has_controller():
                logger.debug("disconnected")
                if self.last_time_not_connected > 0 and self.last_time_pressed > 0:
                    self.gui_controller.trigger_finish()

            self.wait_untill_connected()

            if self.need_to_clear:
                self.gui_controller.trigger_restart()
                self.need_to_clear = False

            if self.gui_controller != None and not self.gui_controller.need_to_clear_gui:
                self.gui_controller.set_title(text='start clicking buttons')
            self.joystick = pygame.joystick.Joystick(0)
            self.joystick.init()
            axes = self.joystick.get_numaxes()
            axes_list = [False] * axes
            for i in range(axes):
                try:
                    axis = self.joystick.get_axis(i)

                    axis_pressed = True if axis != 0 else False

                    if axis_pressed:
                        self.last_time_pressed = time.time()
                        logger.debug("Axis {} value: {:>6.3f}".format(i, axis))

                    if axis_pressed and axis == -1:
                        self.joystick_buttons['axis{}'.format(i + 2)] = axis_pressed
                        axes_list[i + 2] = True
                    elif not axes_list[i]:
                        self.joystick_buttons['axis{}'.format(i)] = axis_pressed

                except KeyError:
                    logging.warning('trying to reach axis {} which doesnt exist'.format(i))
                except IndexError:
                    logging.warning("program determined {} axes but trying to reach axis {}".format(axes, (i + 2)))

            buttons = self.joystick.get_numbuttons()

            for i in range(buttons):
                try:
                    button = self.joystick.get_button(i)
                    button_pressed = True if button != 0 else False
                    if button_pressed:
                        self.last_time_pressed = time.time()
                        logger.debug("Button {:>2} value: {}".format(i, button))
                    self.joystick_buttons['button{}'.format(i)] = button_pressed
                except KeyError:
                    logging.warning('trying to reach button {} which doesnt exist'.format(i))

            if self.gui_controller is not None:
                self.gui_controller.update_buttons(buttons_dictionary=self.joystick_buttons)
            time.sleep(0.01)

    def wait_untill_connected(self):
        joystick_count = pygame.joystick.get_count()
        time_start = time.time()
        while joystick_count == 0:
            time.sleep(0.5)
            if time.time() - time_start > 3:
                if self.gui_controller != None:
                    self.gui_controller.set_title(text='please enter joystick')
                    pygame.joystick.quit()
                    pygame.joystick.init()
                joystick_count = pygame.joystick.get_count()
                if joystick_count:
                    logger.debug("connected")
                    self.need_to_clear = True
