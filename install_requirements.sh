#!/bin/bash

pipenv install pygame
pipenv install screeninfo
pipenv install pillow
python3 setup.py sdist bdist_wheel
pipenv install dist/joysticktester-*.whl

