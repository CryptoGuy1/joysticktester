*****************************
joystick tester
*****************************

A joystick tester for USB PC joysticks

**************
Usage:
**************

$ js_test

or

$ js --log_level DEBUG

<log_levels>
"CRITICAL"
"ERROR"
"WARNING"
"INFO"
"DEBUG"
"NOTSET"

***************************
Creating a pipenv
***************************

# sudo ./create_pipenv.sh


***************************
Deployment
***************************

$ sudo ./build.sh <version_number>