#!/bin/bash

rm *.deb
rm *.tar.gz
apt-get install python3-stdeb fakeroot python-all
python3 setup.py --command-packages stdeb.command sdist_dsc --dist-dir=joysticktester_deb bdist_deb
cp preinst joysticktester_deb/joysticktester-$1/debian/
cd joysticktester_deb/joysticktester-$1/
dpkg-buildpackage -rfakeroot -uc -us -b
cd ../../
cp joysticktester_deb/*deb ./
rm -rf joysticktester_deb/

