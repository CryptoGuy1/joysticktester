#!/bin/bash

apt update
apt install -y python3-pip python3-dev build-essential python3-pil.imagetk
pip3 install --upgrade pip3
pip3 install --upgrade virtualenv
pip3 install pipenv
python3 setup.py sdist bdist_wheel
pipenv install --three
pipenv shell
