#!/usr/bin/env python3

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(
    name='joysticktester',
    version='1.2.1',
    description='joystick tester',
    long_description=readme(),
    packages=['joysticktester'],
    package_data={'joysticktester': ['assets/*.*']},
    url='',
    python_requires=">=3.6",
    license='',
    zip_safe=False,
    author='Guy Milrud',
    author_email='guymilrud@gmail.com',
    scripts=['bin/js_test'],
    install_requires=['']

)
